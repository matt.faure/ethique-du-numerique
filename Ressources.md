# Ressources

## L'éthique

* Congrès Adullact 2018 : [Éthique et logiciel libre, Claude Kirchner (INRIA) 50 minutes](https://invidious.fdn.fr/watch?v=7rcS5WIr15w&autoplay=0&continue=0&dark_mode=true&listen=0&local=0&loop=0&nojs=0&player_style=youtube&quality=medium&thin_mode=false)
* CIGREF : [Éthique et numérique, 2018](https://www.cigref.fr/ethique-numerique-un-referentiel-pratique-pour-les-acteurs-du-numerique)
* CNIL : [Ethique et Intelligence artificielle](https://www.cnil.fr/fr/ethique-et-intelligence-artificielle)

## Vie privée

* Documentaire franco-allemand *Nothing to hide*, 2017
    * [*Nothing to hide* sur Vimeo, 90 minute](https://vimeo.com/193515863), sous licence libre
    * [*Nothing to hide* sur Wikipedia](https://fr.wikipedia.org/wiki/Nothing_to_Hide)
* Livre *Surveillance://*, Tristant Nitot, 2016
    * [*Surveillance://* chez son éditeur CF Éditions](https://cfeditions.com/surveillance/)
    * [*Surveillance://* sur le blog de l'auteur](https://standblog.org/blog/pages/Surveillance)