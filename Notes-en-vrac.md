# Éthique du numérique - Notes en vrac

Registre téléphone portable :

* Mozilla [Désintoxication des données : cinq façons de réinitialiser votre relation avec votre téléphone](https://blog.mozilla.org/firefox/fr/donnees-detox-smartphone/)

Registre Mots de passe : 

* [Firefox Monitor](https://monitor.firefox.com/)

## Plan

Besoin de poser la problématique, et de la faire partager par l'auditoire avant d'amener une quelconque solution. Exemple :

* [Les historiques de navigation sont suffisamment uniques pour identifier les utilisateurs de manière fiable assure Mozilla](https://www.zdnet.fr/actualites/les-historiques-de-navigation-sont-suffisamment-uniques-pour-identifier-les-utilisateurs-de-maniere-fiable-assure-mozilla-39908815.htm)


## Programme imaginé fin 2019


* souveraineté
* éthique du numérique
* vie privée

et aussi

* Navigateur --> Firefox
* Extensions navigateur :
    * UBlock Origin
    * Containers FB, YT, etc
    * Firefox Sync
* moteur de recherche --> Qwant / DuckDuckGo
* modèle la publicité en ligne : ciblage extrême
* implications sur la Société : surveillance / fichage de masse
* Vie privée // réseaux sociaux => grand écart, sauf solutions libres


## Souveraineté

### Definition

Wikipedia : --> souveraineté = indépendance

### Cloud

Cloud = ordinateur de quelqu'un d'autre, que fait le détenteur de vos données avec vos données ?
Alternatives solutions Google et autres : Nextcloud

### outils

* Riot
* NextCloud
* OpenStreetMap

### Fonctionnement du libre

* dons / partage / contribution
* comprendre le fonctionnement => comprendre les imperfections => passer consommateur à contributeur
* contribuer à OpenStreetMap
