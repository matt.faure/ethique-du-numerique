## Contenu

### Pour les particuliers

* Courriel : Pourquoi quitter Gmail, et où aller à la place ? (hint: si c'est gratuit c'est toi le produit)

### Pour les professionnels

* Mesure d'audiance de site web : pourquoi retirer Google Tag Manager / Google Analytics, par quoi le remplacer ? (spoil: Matomo)
    * sites visés : https://village.enercoop.fr/
* Création de sites web : pourquoi retirer les cartes Google Map, et par quoi les remplacer ? (spoil: OpenStreetMap)
* Pour les formulaires, éviter Google Form et TypeForm. Malheureusement, je n'ai pas trouvé d'alternative sexy et ethique à TypeForm (Frapaform ne fait pas le poid d'un point de vue ergonomique)
    * Site visé : LaNef, formulaire de demande de kit de communication https://www.lanef.com/vie-cooperative/parler-de-la-nef/ et https://lanef.typeform.com/to/yHwYuc

## Plan global

* Une fiche par sujet, découpée en deux parties :
    * description du concept (ne changeant pas dans le temps)
    * présentation des alternatives (qui elles peuvent varier d'une année à l'autre)
* Ces fiches sont écrites de manière collaborative, façon wikipedia
* Argumenter avec des sources, chacun des propos afin d'être crédible
* Être particulièrement vigilant au "yakafaucon", tout changement est par nature compliqué et consommateur d'énergie

## Exemples de sujets

* Choisir un courriel éthique
* Avoir un agenda partagé éthique
* Faire un visio éthique
* Choisir un navigateur internet éthique
* Choisir un moteur de recherche éthique
