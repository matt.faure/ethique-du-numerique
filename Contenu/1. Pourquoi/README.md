# Pourquoi

* Raconter une histoire, qui touche les personnes, avec des sensations et des émotions.
  Avoir plusieurs histoires différentes selon les personnes du public
* Constant d'incohérence : des organismes très éthiques dans leur domaine utilisent des outils numériques
  qui ne le sont pas du tout !
  Ex : TN utilise Google Drive, le Graine utilise Zoom, 1083 utilise reCaptcha, LaNef utilise AWS pour stocker les papiers d'identité
* Plaidoyer : fournir des ressources (faits documentés, témoignages, actualités...) pour aider à disséminer l'information
* Les citoyens ne savent pas "lire, écrire, compter" le numérique. Ils n'en ont pas conscience et ne comprennent pas
  l'intérêt de se former sur le sujet. Autre version : mettre des mots sur des ressentis, afin de pouvoir les transmettre à d'autres
* Le niveau d'alphabétisation ud numérique empêche l'individu d'être un citoyen (prendre des décisions éclairées, participer à la démocratie)
* Consom'action / être citoyen