# Projets Similaires

## Université Colibris : Numérique Éthique 

* [Créer un projet collectif : méthodes et outils éthiques](https://colibris-universite.org/formation/creer-un-projet-collectif-methodes-et-outils-ethiques)
* le wiki associé : [Créer des projets collectifs, méthodes et outils éthiques](https://colibox.colibris-outilslibres.org/yeswiki/?NumeriqueEthique)

Et aussi un "parcours citoyen" [Des outils libres pour vos projets collectifs](https://colibris-universite.org/formation/des-outils-libres-pour-vos-projets-collectifs)

## Sites web

* [Je n'ai rien à cacher](https://jenairienacacher.fr/)
* le [Wiki d'Herminien](https://wiki.pcet.link/) (wiki fermé et non-contributif...) et [l'annonce sur LinuxFR](https://linuxfr.org/news/le-wiki-d-herminien-un-wiki-destine-aux-novices-pour-un-numerique-respectueux-et-emancipateur)
