# Quand

* Dès que c'est prêt :)
* Conduite du changement à gérer : bien identifier les freins / les avantages.
  * Bonus en compétences
  * Bonus en lien social
  * Bonus en santé
  * Gain en résilience
