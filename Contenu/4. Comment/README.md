# Comment 

* Une fiche par sujet
* Permettre de se construire un parcours selon les critères facile / puissant

## Exemples de sujets

* le mail // quitter gmail
* le navigateur
* le moteur de recherche
* le transfert de fichiers volumineux
* le mobile :
    * l'opérateur (Telecoop)
    * le périphérique
        * [FairPhone](https://www.fairphone.com/fr/)
        * [PinePhone](https://pine64.com/product-category/smartphones/) (à creuser en premier)
    * l'OS
    * les applis
    * l'agenda partagé

## Exemples de fiche

1. Les concepts (description courte + lien wikipedia) ; c'est intemporel
2. Les outils / services (pouvant changer d'une année à l'autre)
3. Des exemples / arguments étayés et chiffrés


