# Transfert de gros fichier

## Concepts

Le courrier électronique n'est pas conçu techniquement pour transférer de gros fichiers (taille > 5Mo).

## Fournisseurs à remplacer

* Dropbox
  * pourquoi ?
* WeTransfert
* GoogleDrive

## Solutions éthiques

* <https://drop.infini.fr/> (logiciel libre Lufi)
* <https://upload.disroot.org/> (logiciel libre Lufi)
* <https://drop.chapril.org/> (logiciel libre Send)
* <https://www.swisstransfer.com/fr> (logiciel propriétaire)

Autres solutions techniques : 

* Logiciel libre [Jirafeau](https://gitlab.com/mojo42/Jirafeau) : 
  * avec instances <https://fichiers.ouvaton.coop/> et <https://demo.jirafeau.net/> mais aucune condition / doc affichée
  * maintenance / communauté du logiciel ?