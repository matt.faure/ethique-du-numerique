# Email / quitter gmail

## Concepts

* Il est possible de quitter Gmail :)

## Prestataires / services

### Modèle économique

* Un mail coûte cher à faire fonctionner : serveurs, réseaux, ingénieurs. le mail est un objet *vraiment* complexe.
* Un fournisseur de mail gratuit se rémunère forcément quelque part ("Si c'est gratuit c'est toi le produit").

### Manières de consulter son mail

* par une interface web ("webmail")
* par une application sur son ordi: Thunderbird (pendant libre d'Outlook)
* par une application sur son téléphone : K9-email ou appli (libre) du fournisseur

### 1. Trouver un prestataire éthique

Choisir un prestataire dont le modèle économique est lisible et "sain"

Exemples :

* Gandi.net (partenariat Thunderbird)
* MailFence (partenariat Thunderbird)
* Posteo
* ProtonMail
  * [Migrate from Gmail to ProtonMail](https://protonmail.com/support/knowledge-base/transitioning-from-gmail-to-protonmail/)
* LaPoste.net


### 2. Être souverain avec son propre nom de domaine

* prendre un nom de domaine et une offre de mail chez un prestataire
* Exemples de presta : OVH, Gandi, Infomaniak
