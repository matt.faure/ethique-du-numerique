# Ateliers pratiques

* Changer de navigateur
* Changer de moteur de recherche
* Changer de courriel / quitter Gmail
* Quitter Google Drive
* Quitter Whatsapp / bienvenue Element !
* Gérer correctement ses mots de passes
* Quitter Google photo / Picasa. Cas d'usage = je prends une photo sur mon téléphone, elle est synchronisée sur le cloud, en rentrant je peux facilement choisir quelle photo je mets dans un album et je partage le lien aux copains
